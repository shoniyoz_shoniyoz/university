import { createRouter, createWebHistory } from "vue-router";
import Home from "../pages/client/TheHome.vue";
import Talents from "../pages/client/TheTalents.vue";
import About from "../pages/client/TheAbout.vue";
import Contest from "../pages/client/TheContest.vue"
import Press from "../pages/client/ThePress.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/talents",
      name: "talents",
      component: Talents,
    },
    {
      path: "/about",
      name: "about",
      component: About,
    },
    {
      path: "/contest",
      name: "contest",
      component: Contest,
    },
    {
      path: "/press",
      name: "press",
      component: Press,
    },

  ],
});

export default router;
